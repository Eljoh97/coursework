public class TestAccount2 {
    
    public static void main(String[] args) {
        
        Account[] arrayOfAccounts = new Account[5];
        double[] amounts = {23,5444,2,345,34};
        String[] names = {"Picard", "Ryker", "Worf", "Troy", "Data"};

        for(int i = 0; i < arrayOfAccounts.length; i++){
            arrayOfAccounts[i] = new Account();
            arrayOfAccounts[i].setName(names[i]);
            arrayOfAccounts[i].setBalance(amounts[i]);
        }

        Account.setInterestRate(1.5);

        for(int i = 0; i < arrayOfAccounts.length; i++){
            System.out.println("Account name is: " + arrayOfAccounts[i].getName());
            System.out.println("Account balance is: " + arrayOfAccounts[i].getBalance());

            arrayOfAccounts[i].addInterest();

            System.out.println("Account name is: " + arrayOfAccounts[i].getName());
            System.out.println("New account balance is: " + arrayOfAccounts[i].getBalance());
        }

        arrayOfAccounts[4].withdraw();
        System.out.println(arrayOfAccounts[4].withdraw(50));

    }

}