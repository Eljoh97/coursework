public class Account {

    private String name;
    private double balance;
    static double interestRate;

    public static double getInterestRate() {
        return interestRate;
    }

    public static void setInterestRate(double interestRate) {
        Account.interestRate = interestRate;
    }

    public Account(String name, double balance) {
        this.name = name;
        this.balance = balance;
    }

    public boolean withdraw(double amount){
        return (this.getBalance() >= amount);
    }

    public boolean withdraw(){
        if (withdraw(20)){
            this.setBalance(this.getBalance() - 20);
            System.out.println("Transaction successful. Your new balance is $" + this.getBalance());
            return true;
        }
        else{
            System.out.println("Transaction failed");
            return false;
        }
    }

    public Account() {
        this("Jonah", 50);
    }    
    
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public void addInterest(){
        this.balance = this.balance * Account.interestRate;
    }
    
}