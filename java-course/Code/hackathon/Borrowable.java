public interface Borrowable {
    
    public boolean borrow();    // Returns true if item is borrowed, and false if item cannot be borrowed

}