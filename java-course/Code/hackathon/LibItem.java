public abstract class LibItem {
    protected int libID;
    protected boolean availableToBorrow;    // Set to false if an item is currently borrowed from the library
    protected String title;
    protected String description;

    public abstract boolean isAvailable();

    public int getLibID() {
        return libID;
    }

    public void setLibID(int libID) {
        this.libID = libID;
    }

    public boolean getAvailableToBorrow() {
        return availableToBorrow;
    }

    public void setAvailableToBorrow(boolean availableToBorrow) {
        this.availableToBorrow = availableToBorrow;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    
}