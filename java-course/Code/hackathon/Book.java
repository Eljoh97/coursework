public class Book extends LibItem implements Borrowable {
    private String ISBN;

    public Book(int libID, String ISBN, String title, String description){
        this.libID = libID;
        this.ISBN = ISBN;
        this.title = title;
        this.description = description;
        this.availableToBorrow = true;
    }

    public boolean borrow(){
        if (this.isAvailable()){
            this.availableToBorrow = false;
            return true;
        }
        return false;
    }

    public boolean isAvailable(){
        return this.getAvailableToBorrow();
    }

    public String getISBN() {
        return ISBN;
    }

    public void setISBN(String iSBN) {
        ISBN = iSBN;
    }

    
}