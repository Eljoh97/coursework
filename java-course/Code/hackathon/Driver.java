import java.util.jar.Manifest;

public class Driver {

    public static void main(String[] args) {
        
        Library lib = new Library();
    
        Book book1 = new Book(001, "984B62T0", "The First Book", "Wow this is the first book in the library!");

        lib.addItem(book1);

        lib.browse();
    }
}