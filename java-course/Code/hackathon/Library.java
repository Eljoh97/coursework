import java.util.ArrayList;

public class Library {

    private ArrayList<LibItem> collection;

    public Library(){
        this.collection = new ArrayList<LibItem>();
    }

    public void addItem(LibItem item){
        this.collection.add(item);
    }

    public void browse(){
        for (LibItem item : this.collection) {
            if (item.isAvailable()){
                System.out.println(item.getTitle());
            }
        }
    }
}