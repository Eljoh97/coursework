public class TestAccount {
    public static void main(String[] args) {
        
        Account myAccount = new Account();
        myAccount.setName("My Account");
        myAccount.setBalance(100);

        System.out.println("Account name is: " + myAccount.getName());
        System.out.println("Account balance is: " + myAccount.getBalance());
        myAccount.addInterest();
        System.out.println("New account balance is: " + myAccount.getBalance());

        Account[] arrayOfAccounts = new Account[5];
        double[] amounts = {23,5444,2,345,34};
        String[] names = {"Picard", "Ryker", "Worf", "Troy", "Data"};

        for(int i = 0; i < arrayOfAccounts.length; i++){
            arrayOfAccounts[i] = new Account();
            arrayOfAccounts[i].setName(names[i]);
            arrayOfAccounts[i].setBalance(amounts[i]);

            System.out.println("Account name is: " + arrayOfAccounts[i].getName());
            System.out.println("Account balance is: " + arrayOfAccounts[i].getBalance());

            arrayOfAccounts[i].addInterest();

            System.out.println("Account name is: " + arrayOfAccounts[i].getName());
            System.out.println("New account balance is: " + arrayOfAccounts[i].getBalance());
        }


    }
}