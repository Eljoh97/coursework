@Document
public class CompactDisc{
    private ObjectID id;
    private String title;
    private String artist;
    private double price;

    public CompactDisc(String title, String artist, double price){
        this.title = title;
        this.artist = artist;
        this.price = price;
    }

    public CompactDisc(){
        this.title = "Default";
        this.artist = "Default";
        this.price = 10.00;
    }
}