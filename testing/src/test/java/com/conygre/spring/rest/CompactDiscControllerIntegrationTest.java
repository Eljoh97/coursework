package com.conygre.spring.rest;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

import java.util.Optional;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import com.conygre.spring.CompactDiscMongoRestBootApplication;
import com.conygre.spring.entities.CompactDisc;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes=CompactDiscMongoRestBootApplication.class)
public class CompactDiscControllerIntegrationTest {

    public static final String TEST_ID = "5f46a3d545bee629d17fd7b2";

    @Autowired
    private CompactDiscController controller;

    @Test
    public void testFindAll() {
        Iterable<CompactDisc> CDs = controller.findAll();
        Stream<CompactDisc> stream = StreamSupport.stream(CDs.spliterator(), false);
        assertThat(stream.count(), equalTo(1L));
    }
    
    @Test
    public void testCdById() {
        Optional<CompactDisc> CD = controller.getCdById(TEST_ID);
        assertThat(CD.isPresent(), equalTo(true));
    }
}