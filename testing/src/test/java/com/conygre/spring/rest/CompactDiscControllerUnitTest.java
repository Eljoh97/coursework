package com.conygre.spring.rest;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import com.conygre.spring.data.CompactDiscRepository;
import com.conygre.spring.entities.CompactDisc;
import com.conygre.spring.service.CompactDiscService;

import org.bson.types.ObjectId;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes=CompactDiscControllerUnitTest.Config.class)
public class CompactDiscControllerUnitTest {

    public static final String TEST_ID = "5f46a3d545bee629d17fd7b2";

    @Configuration
    public static class Config {

        // We really shouldn't need this, but when Spring tries to configure a
        // CompactDiscService bean -- even a mock one! it insists on resolving
        // the @Autowired dependency to a CompactDiscRepository.
        @Bean
        public CompactDiscRepository repo() {
            return mock(CompactDiscRepository.class);
        }

        @Bean
        public CompactDiscService service() {

            ObjectId ID = new ObjectId(TEST_ID);
            CompactDisc CD = new CompactDisc("", "", 0);
            List<CompactDisc> CDs = new ArrayList<>();
            CDs.add(CD);

            CompactDiscService service = mock(CompactDiscService.class);
            when(service.getCatalog()).thenReturn(CDs);
            when(service.getCompactDiscById(ID)).thenReturn(Optional.of(CD));
            return service;
        }

        @Bean
        public CompactDiscController controller() {
            return new CompactDiscController();
        }
    }

    @Autowired
    private CompactDiscController controller;
    
    @Test
    public void testFindAll() {
        Iterable<CompactDisc> CDs = controller.findAll();
        Stream<CompactDisc> stream = StreamSupport.stream(CDs.spliterator(), false);
        assertThat(stream.count(), equalTo(1L));
    }
    
    @Test
    public void testCdById() {
        Optional<CompactDisc> CD = controller.getCdById(TEST_ID);
        assertThat(CD.isPresent(), equalTo(true));
    }
}