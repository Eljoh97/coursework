package com.conygre.spring.rest;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.notNullValue;

import java.util.List;

import com.conygre.spring.entities.CompactDisc;

import org.junit.Test;
import org.springframework.web.client.RestTemplate;

public class CompactDiscControllerFunctionalTest {

    private RestTemplate template = new RestTemplate();

    @Test
    public void testFindAll() {
        @SuppressWarnings("unchecked") // specifying raw List.class
        List<CompactDisc> CDs = template.getForObject("http://localhost:8081/api/compactdiscs", List.class);

        assertThat(CDs, hasSize(1));
    }
    
    @Test
    public void testCdById() {
        CompactDisc CD = template.getForObject
            ("http://localhost:8081/api/compactdiscs/5f46a3d545bee629d17fd7b2", CompactDisc.class);
        assertThat(CD, notNullValue());
    }
}