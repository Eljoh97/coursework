/*
Copyright 2016 Will Provost.
All rights reserved by Capstone Courseware, LLC.
*/

package com.citi.testing;

import static com.citi.testing.PerfectSquare.perfectSquare;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hamcrest.Matcher;
import org.junit.Test;

/**
Test class that exercises built-in Hamcrest matchers,
and a couple of custom matchers.

@author Will Provost
*/
public class Hamcrest
{
		/**
		 * Factory for a Hamcrest matcher that requires a number to be
		 * between 100 and 999 and a perfect square.
		 */
		public static Matcher<Integer> threeDigitSquare() {
				return allOf(greaterThan(99), lessThan(1000), perfectSquare());
		}

    /**
    Whip up a bunch of primitives, strings, and JavaBean instances,
    and assert various things about them.
    */
    @Test
    public void testHamcrestMatchers() throws Exception
    {
        String goodBoy = "Every good boy does fine.";

        assertThat (goodBoy, isA (String.class));
        assertThat (goodBoy, startsWith ("Ev"));
        assertThat (goodBoy, containsString ("boy"));
        assertThat (goodBoy, endsWith ("fine."));
        assertThat (goodBoy,
            both (startsWith ("Every")).and (endsWith (".")));
        assertThat (goodBoy, either (emptyString ()).or (containsString ("boy")));

        Date earlier = new Date (1471001168000L);
        Date later = new Date (1471001169000L);
        assertThat (earlier, lessThan (later));

        List<Integer> numbers = new ArrayList<>();
        Collections.addAll (numbers, 5, 4, 3, 2, 1);

        assertThat (numbers, hasSize (5));
        assertThat (numbers, hasItem (is (5)));
        assertThat (numbers, everyItem (greaterThan (0)));
        assertThat (numbers, everyItem (lessThan (10)));
        assertThat (numbers, containsInAnyOrder (1, 2, 3, 4, 5));
        assertThat (numbers, anyOf (hasItem (1), hasItem (-1)));

        Map<String, Double> probabilities = new HashMap<>();
        probabilities.put ("Heads", .5001);
        probabilities.put ("Tails", .4999);

        assertThat (probabilities, hasKey ("Heads"));
        assertThat (probabilities, hasValue (.5001));
        assertThat (probabilities.get ("Heads") +
            probabilities.get ("Tails"), closeTo (1.00, 0.0001));

        MyBean expected = new MyBean ("one", 1);
        MyBean actual = new MyBean ("one", 1);
        assertThat (actual, samePropertyValuesAs (expected));

        MyBean firstDelegate = new MyBean ("delegate", 1000);
        MyBean first = new MyBean ("main", 100);
        first.setDelegate (firstDelegate);

        // MyBean secondDelegate = new MyBean ("delegate", 1000);
        MyBean second = new MyBean ("main", 100);
        second.setDelegate (firstDelegate); // secondDelegate won't work

        assertThat (first, is (second));
        assertThat (first, samePropertyValuesAs (second));

        assertThat (9, perfectSquare ());

        assertThat(169, threeDigitSquare());
    }
}
